

define(['jquery', '../lib/kinetic', '../lib/jquery.browserLanguage'], function(jQuery, Kinetic){

    jQuery.noConflict();
    var $j = jQuery;

    var App = Class.create();
    App.prototype = {

        //Constructor
        initialize: function(){

            var self = this;

            //Disable right click default event
            /*
            window.oncontextmenu = function(event) {
                event.preventDefault();
                event.stopPropagation();
                return false;
            };*/

            this.Ressources = new Object();
            this.chargement = 0;
            this.chargementMax = 1;
            this.manageLoadingInterval = undefined;
            this.Config = this.getConfig();
            this.Type = this.generateType();


            //Scale system
            $j(window).resize(function(){ 
                $j('#container').css({ "width": "80%", "height": "80%"});

                //Get windows size
                var width = $j(this).width();
                var height = $j(this).height();

                //Get container size anc calcul for 16/9 size
                var widthC = $j('#container')[0].scrollWidth;
                var heightC = $j('#container')[0].scrollHeight;
                var newWidthC = heightC * (16/9);
                var newHeightC = widthC / (16/9);

                //Set width of container relative as height
                $j('#container').css({
                    "width": newWidthC,
                    "height": heightC
                });

                //if the page is too small
                //Set width of container relative as width
                if(width < $j('#container')[0].scrollWidth)
                {
                    $j('#container').css({
                        "width": widthC,
                        "height": newHeightC
                    });
                }
                
                //Set the margin top for vertical align
                var marginTop = (height - $j('#container')[0].scrollHeight) / 2;
                $j('#container').css("margin-top", marginTop);
                    
                //Send to KOL the fact we need to resize canvas and update scale
                var scale = $j('#container').width() / 1024;
                $j(document).trigger('resizeWindow', [$j('#container').width(), $j('#container').height(), scale]);                
            });
            $j(window).trigger('resize');

            //Detect language and load file language
            this.chargement++;
            $j.browserLanguage(function(language){
                var urlTradFile = undefined;
                switch(language)
                {
                    case "French":
                        urlTradFile = "translations/fr.json";
                        self.Config.language = "French";
                        self.chargement--;
                        break;

                    case "English":
                    default:
                        urlTradFile = "translations/en.json";
                        self.Config.language = "English";
                        self.chargement--;
                        break;
                }
                $j.ajax({
                    url: urlTradFile,
                    cache: false
                }).done(function(trad) {
                    self.t = trad;
                });
            });

        },

        //Init Connection server node JS
        init: function(){
            /* PAS DE NODEJS POUR LE MOMENT
            var baseURL = this.getBaseURL();
            var socket = io.connect(baseURL.substr(0, baseURL.length - 1) + ':8000');
            socket.sessions = new Object();
            return socket;*/
         },

        //Configurations du jeu (accesible via app.Config.XXX)
        getConfig: function(){
            var self = this;
            var configuration = new Object();

            //URLs
            configuration.baseURL = this.getBaseURL();
            configuration.clientURL = configuration.baseURL + "";

            //FOLDERS
            configuration.imagePath = "img/";

            //Canvas
            configuration.widthCanvas = 1024;
            configuration.heightCanvas = 576;

            return configuration;
        },

        //Types of ressources (app.Type.XXX)
        generateType: function(){
            var self = this;
            var type = new Object();

            //TYPES
            type.IMAGE = "image";
            type.SOUND = "sound";

            return type;
        },

        //Use to get actual url
        getBaseURL: function() {
            var url = location.href;  // entire url including querystring - also: window.location.href;
            var baseURL = url.substring(0, url.indexOf('/', 14));

            if (baseURL.indexOf('http://localhost') != -1)
            {
                // Base Url for localhost
                var url = location.href;  // window.location.href;
                var pathname = location.pathname;  // window.location.pathname;
                var index1 = url.indexOf(pathname);
                var index2 = url.indexOf("/", index1 + 1);
                var baseLocalUrl = url.substr(0, index2);

                return baseLocalUrl + "/";
            }
            else
            {
                // Root Url for domain name
                return baseURL + "/";
            }
        },

        //Center the windows to tops
        center: function() {
            window.scrollTo(0, 1);
        },

        //Load all the ressources (images, sounds)
        loadRessources: function() {

            var self = this;

            /* Load Images */
            //this.addRessource(key, type, path);

            //SHARED
                this.addRessource("modal_window", self.Type.IMAGE, "share/modal_window.png");
                this.addRessource("cross_modal_window", self.Type.IMAGE, "share/cross_modal_window.png");
                
            //MENU
                this.addRessource("menu_background", self.Type.IMAGE, "menu/menu_background.png", [
                    "0,0-1024,576", "1024,0-1024,576", "2048,0-1024,576", "0,0-3072,576"
                ]);
                this.addRessource("logo_lastmove", self.Type.IMAGE, "menu/logo_lastmove.png");
                this.addRessource("flags", self.Type.IMAGE, "menu/flags.png", [
                    "0,0-30,24", "30,0-30,24"
                ]);
                this.addRessource("fr_menu_buttons", self.Type.IMAGE, "menu/fr_menu_buttons.png", [
                    "0,0-233,58", "233,0-233,58",
                    "0,58-233,58", "233,58-233,58",
                    "0,116-233,58", "233,116-233,58",
                    "0,174-233,58", "233,174-233,58",
                    "0,232-233,58", "233,232-233,58"
                ]);


            /* Load Sounds */

            //Launch loading screen
            this.chargementMax += this.chargement;
            this.manageLoadingInterval = window.setInterval(function(){ self.manageLoading(self.chargement, self.chargementMax)}, 100);
        },

        //Loading screen managers
        manageLoading: function(load, loadMax) {
            var self = this;

            if(this.chargement == 0)
            {
                $j('#loading_window').fadeOut(function(){
                    $j('#section_canvas').fadeIn();
                    window.clearInterval(self.manageLoadingInterval);
                    $j(document).trigger('loadRessourcesOver');
                });
            }
            else
            {
                var pourcentage = Math.round(100 - (load * 100 / loadMax));
                $j('#pourcentage').html(pourcentage + " %");
            }
        },

        //Use to add ressources (image or sound)
        addRessource: function(key, type, source, sprites) {
            var self = this;

            if (type == self.Type.IMAGE){
                var imgSrc = self.Config.imagePath + source;
                this.Ressources[key] = new Image();
                this.Ressources[key].src = imgSrc;
                if(sprites != undefined)
                    this.Ressources[key].sprites = sprites;
                this.chargement++;
                this.Ressources[key].onload = function(){
                    //Decrease loading
                    self.chargement--;
                };
            }
        },

        getCrop: function(img, key){
            if(img.sprites != undefined)
            {
                var crop = img.sprites[key];
                var coords = crop.split('-');
                var c1 = coords[0].split(',');
                var c2 = coords[1].split(',');
                return {x: c1[0], y: c1[1], w: c2[0], h: c2[1]};
            }
            else
            {
                return {x: 0, y: 0, w: img.width, h: img.height};
            }
        }

    };

    return App;
});