

define(['jquery', '../entity/buttonMenu', '../entity/buttonFlag'], function(jQuery, ButtonMenu, ButtonFlag) {

    jQuery.noConflict();
    var $j = jQuery;
    var MenuController = Class.create();
    MenuController.prototype = {

        initialize: function(){
            //Flags
            this.flagFrench = undefined;
            this.flagEnglish = undefined;
        },

        start: function() {
            var self = this;

            //Background + Logo
            canvas.putGifBackground(app.Ressources["menu_background"], 200);
            canvas.putImage({X: 520, Y: 20}, app.Ressources["logo_lastmove"], "gui");

            //Buttons Menu
            var btnStory = new ButtonMenu("story", 0, 1);
            var btnChallenges = new ButtonMenu("challenges", 2, 3);
            var btnOptions = new ButtonMenu("options", 4, 5);
            var btnAbout = new ButtonMenu("about", 6, 7);
            var btnQuit = new ButtonMenu("quit", 8, 9);
            canvas.putImage({X: 580, Y: 200}, app.Ressources["fr_menu_buttons"], "gui", btnStory, 0);
            canvas.putImage({X: 580, Y: 260}, app.Ressources["fr_menu_buttons"], "gui", btnChallenges, 2);
            canvas.putImage({X: 580, Y: 320}, app.Ressources["fr_menu_buttons"], "gui", btnOptions, 4);
            canvas.putImage({X: 580, Y: 380}, app.Ressources["fr_menu_buttons"], "gui", btnAbout, 6);
            canvas.putImage({X: 580, Y: 440}, app.Ressources["fr_menu_buttons"], "gui", btnQuit, 8);

            //Flags
            self.flagFrench = new ButtonFlag("French");
            self.flagEnglish = new ButtonFlag("English");
            canvas.putImage({X: 940, Y: 10}, app.Ressources["flags"], "gui", self.flagFrench, 1);
            canvas.putImage({X: 980, Y: 10}, app.Ressources["flags"], "gui", self.flagEnglish, 0);
            
            //Language
            self.setLanguageSelected();
            $j(document).on('language_change', function(){
                self.setLanguageSelected();
            });
            
        },

        setLanguageSelected: function(){
            this.flagFrench.setUnselected();
            this.flagEnglish.setUnselected();
            switch(app.Config.language)
            {
                case "French":
                    this.flagFrench.setSelected();
                    break;

                case "English":
                default:
                    this.flagEnglish.setSelected();
                    break;
            }
        }

            
    };

    return MenuController;

});
