
define(['../lib/kol'], function(KOL){

    var ButtonMenu = Class.create();
    ButtonMenu.prototype = {

            initialize:function(type, keySample, keyPressed){
                this.type = type;
                this.image = undefined;
                this.keySample = keySample;
                this.keyPressed = keyPressed;
            },

            onMouseOver: function (event){
                //this.image.attrs.image = this.imagePressed;
                var crop = app.getCrop(this.image.attrs.image, this.keyPressed);
                this.image.setCrop({
                    x: crop.x,
                    y: crop.y,
                    width: crop.w,
                    height: crop.h
                });
            },

            onMouseOut: function(event){
                //this.image.attrs.image = this.imageSample;
                var crop = app.getCrop(this.image.attrs.image, this.keySample);
                this.image.setCrop({
                    x: crop.x,
                    y: crop.y,
                    width: crop.w,
                    height: crop.h
                });
            },

            onClick: function(event){
                if(this.type == "quit")
                {
                    canvas.showModalWindow(app.t["menu"]["quitMessage"], "OK");
                }
            }

    };

    return ButtonMenu;
});
