
define(['jquery', '../lib/kol'], function(jQuery, KOL){

    jQuery.noConflict();
    var $j = jQuery;

    var ButtonFlag = Class.create();
    ButtonFlag.prototype = {

            initialize:function(language){
                this.language = language;
                this.image = undefined;
            },

            onClick: function(event){
                app.Config.language = this.language;
                $j(document).trigger('language_change');
            },

            setSelected: function(){
                this.image.setStrokeWidth(2);
                this.image.setStroke("White");
            },

            setUnselected: function(){
                this.image.setStrokeWidth(0);
                this.image.setStroke("Transparent");
            }

    };

    return ButtonFlag;
});
