define(['jquery', './vector2', './tweenlite'], function(jQuery, Vector2){

    jQuery.noConflict();
    var $j = jQuery;

    /* Kinnetic Overlay Library : KOL */
    var KOL = Class.create();
    KOL.prototype = {
        initialize: function(idContainer, x, y, scale){

            var self = this;
            
            //Stage Kinnetic
            this.stage = new Kinetic.Stage({
                container: idContainer,
                width: x,
                height: y
            });
            this.w = app.Config.widthCanvas;
            this.h = app.Config.heightCanvas;
            this.width = function(){ return self.w; }
            this.height = function() { return self.h; }

            //Scale system
            $j(document).on('resizeWindow', function(event, x, y, scale){
                self.stage.setWidth(x);
                self.stage.setHeight(y);
                self.stage.setScale(scale);
            });
            this.stage.setScale(scale);

            //Layers Names
            this.L_NAME = new Object();
            this.L_NAME.background = "background";
            this.L_NAME.gui = "gui";
            this.L_NAME.modal = "modal";

            //Create Layers (order z_index)
            this.layers = new Object();
            this.layers[this.L_NAME.background] = new Kinetic.Layer();
            this.layers[this.L_NAME.gui] = new Kinetic.Layer();
            this.layers[this.L_NAME.modal] = new Kinetic.Layer();

            //Add each layers in stage
            for (var i in this.layers){
                this.stage.add(this.layers[i]);
            }

            //Use to display FPS Framerate
            this.timeFPS = Date.now();

            //Launch refresh canvas 60 FPS
            this.drawLoopInterval = window.setInterval(function(){ self.draw(); }, 1000 / 60);

            //Interval drawing
            this.backgroundGif = undefined; //Use une case of background gif simulation
        },

        draw: function() {
            //Display FPS
            var now = Date.now();
            var diff = now - this.timeFPS;
            var fps = 1000 / diff;
            $j("#FPS").html(Math.floor(fps));
            this.timeFPS = now;

            //Draw layer
            for (var i in this.layers){
                this.layers[i].draw();
            }
        },

        clearCanvas: function() {
            for (var i in this.layers){
                this.layers[i].removeChildren();
            }
        },

        putBackground: function(imageObj, key) {
            var self = this;
            var crop = app.getCrop(imageObj, key);
            var background = new Kinetic.Image({
                image: imageObj,
                x: 0,
                y: 0,
                width: crop.w,
                height: crop.h,
                crop: {
                    x: crop.x,
                    y: crop.y,
                    width: crop.w,
                    height: crop.h
                }
            });

            this.layers[self.L_NAME.background].removeChildren();
            this.layers[self.L_NAME.background].add(background);
        },

        putGifBackground: function(image, time, key) {
            var self = this;
            if(key == undefined)
            { 
                key = 0;
                self.putBackground(image, key); 
            }
            this.backgroundGif = setTimeout(function(){
                self.putBackground(image, key);
                self.putGifBackground(image, time, (key+1)%(image.sprites.length-1));
            }, time);
        },

        putImage: function(vector, imageObj, layerName, objectLinked, spriteKey, zIndex){
            var self = this;

            var customImg = undefined;
            if(spriteKey != undefined)
            {
                var crop = app.getCrop(imageObj, spriteKey);
                customImg = new Kinetic.Image({
                    image: imageObj,
                    x: vector.X,
                    y: vector.Y,
                    width: crop.w,
                    height: crop.h,
                    crop: {
                        x: crop.x,
                        y: crop.y,
                        width: crop.w,
                        height: crop.h
                    }
                });
            }
            else
            {
                customImg = new Kinetic.Image({
                    image: imageObj,
                    x: vector.X,
                    y: vector.Y
                });
            }

            objectLinked = (objectLinked == undefined) ? new Object() : objectLinked;
            customImg.objectLinked = objectLinked;
            objectLinked.image = customImg;

            if(this.layers[layerName] == undefined)
            {
                this.layers[layerName] = new Kinetic.Layer();
            }

            this.layers[layerName].add(customImg);

            zIndex = (zIndex == undefined) ? 0 : zIndex;
            customImg.setZIndex(zIndex);

            self.manageEvent(customImg);
        },

        manageEvent: function(image) {
            var self = this;

            if(image.objectLinked == undefined)
                return;

            if(typeof image.objectLinked.onMouseOut === 'function')
            {
                image.on('mouseout', function(e){
                    image.objectLinked.onMouseOut(e);
                });
            }

            if(typeof image.objectLinked.onMouseOver === 'function')
            {
                image.on('mouseover', function(e){
                    image.objectLinked.onMouseOver(e);
                });
            }

            if(typeof image.objectLinked.onClick === 'function')
            {
                image.on('click', function(e){
                    image.objectLinked.onClick(e);
                });
            }

        },

        //buttonType: OK, OKCANCEL, YESNO
        showModalWindow: function(text, buttonType){
            var self = this;

            //Black modal rectangle
            var rectModal = new Kinetic.Rect({
                x: 0,
                y: 0,
                width: self.width(),
                height: self.height(),
                fill: 'black',
                opacity: 0.7
            });
            this.layers[this.L_NAME.modal].add(rectModal);
            
            //PopUp Window
            var imgModalWindow = app.Ressources["modal_window"];
            var modalWindow = new Kinetic.Image({
                image: imgModalWindow,
                x: self.width() / 2 - imgModalWindow.width / 2,
                y: self.height() / 2 - imgModalWindow.height / 2
            });
            this.layers[this.L_NAME.modal].add(modalWindow);

            //Text
            var message = new Kinetic.Text({
                x: self.width() / 2,
                y: modalWindow.getY() + 40,
                text: text,
                fontSize: 20,
                fontFamily: 'Calibri',
                fill: '#313131'
            });
            message.setOffset({
                x: message.getWidth() / 2
            });
            this.layers[this.L_NAME.modal].add(message);

            //Cross quit
            var cross = new Kinetic.Image({
                image: app.Ressources["cross_modal_window"],
                x: self.width() / 2 + modalWindow.getWidth() / 2 - app.Ressources["cross_modal_window"].width * 2 - 10,
                y: modalWindow.getY() + 10
            });
            cross.on('click', function(){
                self.layers[self.L_NAME.modal].removeChildren();
            });
            this.layers[this.L_NAME.modal].add(cross);
        }

    };

	
    return KOL;
});
