/**
 * User: Maxime
 */

define(['jquery', 'lib/kol', 'controllers/app', 'controllers/menuController'], function(jQuery, KOL, App, MenuController) {

    jQuery(function($) {

        app = undefined;
        canvas = undefined;
        var menuController;

        //Load the App (html part)
        var initApp = function() {

            app = new App();

            //Load Controllers
            menuController = new MenuController();

            //Call init configurations
            app.center();
            app.loadRessources();

            var first = true;
            $(document).on('loadRessourcesOver', function() {
                if(first)
                {
                    first = false;

                    //Log+ContinueLoad
                    initGame();
                }
            });
        };

        //Load the Game (canvas part)
        var initGame = function() {

            //Size container
            var widthC = $('#container').width();
            var heightC = $('#container').height();
            var scale = $('#container').width() / 1024;

            //Draw Game Canvas
            canvas = new KOL("section_canvas", widthC, heightC, scale);

            //Call first controller
            menuController.start();
        };

        initApp();

    });

});
