
<h1>Welcome to translator !</h1>

<?php

/* WHAT IS translator.php ?
 * translator.php take two names of json files as get parameters (from and to),
 * then he update the second file (to) base on the keys found in the first (from). 
 * Example: "translator.php?from=fr&to=en" with fr.json file and en.json file
 * NB: the files as to be ine the same folder than the script
 *
 * HOW IT WORK ?
 * Read comments for more details. Here a summary:
 * - Open the two file and convert contents in php var.
 * - Iterate through the "from" file and looking for the key in the "to" file
 * - if key is present do nothing, if not present add the key
 * - the script manage the depth of the json if the value of a key is an array (max: 4)
 * - the script put the missings key at the right place
 */

//error handler
set_error_handler( 'error_handler' );
function error_handler( $errno, $errmsg, $filename, $linenum, $vars )
{
    if ( 0 === error_reporting() )
      return false;
    if ( $errno !== E_ERROR )
      throw new \ErrorException( sprintf('%s: %s', $errno, $errmsg ), 0, $errno, $filename, $linenum );
}

if(isset($_GET['from']) && isset($_GET['to']))
{
	//Open files
	$content_from = null;
	$content_to = null;
	try {
		$content_from = file_get_contents($_GET['from'] . ".json");
	} catch(Exception $e) {
		echo "Error at opening file " . $_GET['from'] .".json : NO FILE FOUND";
		return;
	}
	try {
		$content_to = file_get_contents($_GET['to'] . ".json");
	} catch(Exception $e) {
		echo "File " . $_GET['to'] .".json not found, then created !<br />";
		file_put_contents($_GET['to'] . ".json", "");
		$content_to = "";
	}

	//Get json content as php var
	$from = null;
	$to = null;
	try {
		$from =json_decode($content_from,true);
		$to =json_decode($content_to,true);
		if($from == null)
			throw new Exception("Parsing error detected in the \"from\" file !");
		if($to == null)
			$to = array();
	} catch(Exception $e) {
		echo "Error at converting json content : <br />" . $e . "<br/> Be free to use a validator : http://jsonlint.com/";
		return;
	} 

	$error = false;
	
	//Something dirty here
	//Iterate through $from for looking for key not present in $to
	$i1 = 0;
	foreach ($from as $key => $val) {
		if(is_array($val)) {
			if(!isset($to[$key]))
				$to[$key] = array();
			$i2 = 0;
			foreach ($val as $key2 => $val2) {
				if(is_array($val2)) {
					if(!isset($to[$key][$key2]))
						$to[$key][$key2] = array();
					$i3 = 0;
					foreach ($val2 as $key3 => $val3) {
						if(is_array($val3)) {
							if(!isset($to[$key][$key2][$key3]))
								$to[$key][$key2][$key3] = array();
							$i4=0;
							foreach ($val3 as $key4 => $val4) {
								if(is_array($val4)) {
									if(!isset($to[$key][$key2][$key3][$key4]))
										$to[$key][$key2][$key3][$key4] = "TODO " . $val4;
									
									echo "Too much depth in the json. (Max: 4)";
									$error = true;
									
								} else {
									if(!isset($to[$key][$key2][$key3][$key4]))
									{
										$to[$key][$key2][$key3] = array_slice($to[$key][$key2][$key3], 0, $i4, true) +
					            		array($key4 => "TODO " . $val4) +
					            		array_slice($to[$key][$key2][$key3], $i4, NULL, true);
									}
								}
								$i4++;
							}
						} else {
							if(!isset($to[$key][$key2][$key3]))
							{
								$to[$key][$key2] = array_slice($to[$key][$key2], 0, $i3, true) +
					            array($key3 => "TODO " . $val3) +
					            array_slice($to[$key][$key2], $i3, NULL, true);
							}
						}
						$i3++;
					}
				} else {
					if(!isset($to[$key][$key2]))
					{
			            $to[$key] = array_slice($to[$key], 0, $i2, true) +
			            array($key2 => "TODO " . $val2) +
			            array_slice($to[$key], $i2, NULL, true);
					}
				}
				$i2++;
			}
		} else {
			if(!isset($to[$key]))
			{
				$to = array_slice($to, 0, $i1, true) +
	            array($key => "TODO " . $val) +
	            array_slice($to, $i1, NULL, true);
			}
		}
		$i1++;
	}

	//Convert $to var in $json and put the generate json in the destination file
	$jsonToFile = json_encode($to, JSON_PRETTY_PRINT); //JSON_PRETTY_PRINT >= PHP 5.4.0 needed
	$fileTo = fopen($_GET['to'] . ".json", 'w+');
	fputs($fileTo, $jsonToFile);

	//Display state of translate
	if(!$error)
		echo "Translate file updated successfully ! (from \"" . $_GET['from'] . "\" to \"" . $_GET['to'] . "\")";
	else
		echo "Some errors has been detected !";

}
else
{
	//In case url parameters not valid
	echo "Error: malformed URL. Refere to below: <br/>";
	echo "translator.php?from=*base_file*&to=*to_update_file*<br/>*base_file*: file take as reference <br/>*to_update_file*: file to update base on the base_file.";
}



?>